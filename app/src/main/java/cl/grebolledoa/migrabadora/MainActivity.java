package cl.grebolledoa.migrabadora;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CallLog;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.READ_CALL_LOG;

public class MainActivity extends AppCompatActivity {

    Button btGrabar, btPararGrabacion, btReproducir, btPararReproduccion, btCamara;
    ImageView ivLogo;
    String audioPath = null;
    Random random;
    String randomCaracterFileName = "ABCDEFGHYJKLMNOP";
    MediaRecorder mediaRecorder;
    MediaPlayer mediaPlayer;

    final int requestCodePermision = 1;
    final int requestCodePermisionCallLog = 2;

    final int requestCodeCamera = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(this.isTablet()){
            setContentView(R.layout.activity_main_xl);
        }else{
            setContentView(R.layout.activity_main);
        }

        this.btGrabar = findViewById(R.id.btn_grabar);
        this.btPararGrabacion = findViewById(R.id.btn_parar_grabacion);
        this.btReproducir = findViewById(R.id.btn_reproducir);
        this.btPararReproduccion = findViewById(R.id.btn_parar_reproduccion);
        this.btCamara = findViewById(R.id.bt_camara);
        this.ivLogo = findViewById(R.id.iv_logo);

        this.btPararGrabacion.setEnabled(false);
        this.btReproducir.setEnabled(false);
        this.btPararReproduccion.setEnabled(false);

        this.random = new Random();

        this.btGrabar.setOnClickListener(v -> {
            if(chequearPermisos()){
                //logica para empezar a grabar el audio
                audioPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                        crearNombreAudio(5) + "AudioRecording.3gp";

                mediaRecoderReady();

                try{
                    this.mediaRecorder.prepare();
                    this.mediaRecorder.start();
                }catch(Exception e){
                    e.printStackTrace();
                }

                this.btGrabar.setEnabled(false);
                this.btPararGrabacion.setEnabled(true);
                Toast.makeText(this, "Grabación iniciada", Toast.LENGTH_SHORT).show();
            }else{
                solicitarPermisos();
            }
        });

        this.btPararGrabacion.setOnClickListener(v -> {
            mediaRecorder.stop();
            this.btPararGrabacion.setEnabled(false);
            this.btGrabar.setEnabled(true);
            this.btReproducir.setEnabled(true);

            Toast.makeText(this, "Grabación detenida", Toast.LENGTH_SHORT).show();
        });

        this.btReproducir.setOnClickListener(v -> {
            this.btReproducir.setEnabled(false);
            this.mediaPlayer = new MediaPlayer();

            try{
                this.mediaPlayer.setDataSource(this.audioPath);
                this.mediaPlayer.prepare();
                this.mediaPlayer.start();
                this.btPararReproduccion.setEnabled(true);
            }catch(Exception e){
                e.printStackTrace();
            }
        });

        this.btPararReproduccion.setOnClickListener(v -> {
            btPararReproduccion.setEnabled(false);
            this.btGrabar.setEnabled(true);

            if(mediaPlayer != null){
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaRecoderReady();
            }
        });


        ActivityResultLauncher<Intent> activityResultLauncher =
                registerForActivityResult(
                        new ActivityResultContracts.StartActivityForResult(),
                        result -> {
                            if(result.getResultCode() == RESULT_OK){
                                Bundle extras = result.getData().getExtras();
                                Bitmap imageBitMap = (Bitmap) extras.get("data");
                                this.ivLogo.setImageBitmap(imageBitMap);
                            }
                        }
                );

        this.btCamara.setOnClickListener(v -> {
            Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            activityResultLauncher.launch(intentCamera);
        });

        ObtenerDatosLlamadas();
    }

    public void ObtenerDatosLlamadas(){

        if(ContextCompat.checkSelfPermission(this,
                READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED ){
            Uri uri;
        /*
            content://media/internal/images
            content://media/internal/video
            content://media/internal/audio
            content://call_log/calls
         */

            uri = Uri.parse("content://call_log/calls");

            String [] projeccion = new String[] {CallLog.Calls.TYPE,
                    CallLog.Calls.NUMBER, CallLog.Calls.DURATION};

            String clauseSelection = CallLog.Calls.DURATION + "=0";


            Cursor c = getContentResolver().query(
                    uri,
                    projeccion,
                    null,
                    null,
                    null
            );

            while(c.moveToNext()){
                Log.i("calls", c.getString(0)+ " " + c.getString(1) + " " + c.getString(2));
            }

            c.close();
        }else{
            solicitarPermisosCallLog();
        }
    }




    public void mediaRecoderReady(){
        this.mediaRecorder = new MediaRecorder();
        this.mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        this.mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        this.mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        this.mediaRecorder.setOutputFile(this.audioPath);
    }

    private boolean isTablet(){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float yInches= metrics.heightPixels/metrics.ydpi;
        float xInches= metrics.widthPixels/metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
        if (diagonalInches>=6.5){
            return true;
            // 6.5inch device or bigger
        }else{
            // smaller device
            return false;
        }
    }

    private String crearNombreAudio(int largoNombre){
        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
        while(i < largoNombre){

            int randomposition = random.nextInt(randomCaracterFileName.length());

            stringBuilder.append(
                    randomCaracterFileName.charAt(randomposition)
            );
            i++;
        }

        return stringBuilder.toString();
    }

    private void solicitarPermisos() {
        ActivityCompat.requestPermissions(this,
                new String[]{ WRITE_EXTERNAL_STORAGE, RECORD_AUDIO },
                requestCodePermision);
    }

    private void solicitarPermisosCallLog() {
        ActivityCompat.requestPermissions(this,
                new String[]{ READ_CALL_LOG },
                requestCodePermisionCallLog);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int grantResult[]){
        switch (requestCode){
            case requestCodePermision:
                if(grantResult.length > 0){
                    boolean storagePermission = grantResult[0] == PackageManager.PERMISSION_GRANTED;
                    boolean recordPermission = grantResult[1] == PackageManager.PERMISSION_GRANTED;

                    if(storagePermission && recordPermission){
                        Toast.makeText(this, "Permisos concedidos", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this, "Permisos degenados", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case requestCodePermisionCallLog:
                if(grantResult.length > 0){
                    boolean callLogPermission = grantResult[0] == PackageManager.PERMISSION_GRANTED;

                    if(callLogPermission){
                        Toast.makeText(this,"Permiso concedido", Toast.LENGTH_SHORT).show();
                    }
                }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResult);
    }

    public boolean chequearPermisos(){
        int resultWriteExternalStorage = ContextCompat.checkSelfPermission(this,
                WRITE_EXTERNAL_STORAGE);

        int resultRecordAudio = ContextCompat.checkSelfPermission(this,
                RECORD_AUDIO);

        return resultWriteExternalStorage == PackageManager.PERMISSION_GRANTED &&
                resultRecordAudio == PackageManager.PERMISSION_GRANTED;

    }

}